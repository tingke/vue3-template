import request from './request';

export const userList = params => {
    return request({
        url: '/users',
        params
    });
};
