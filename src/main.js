import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import SvgIcon from '@/icons';
import i18n from '@/i18n';
import fitlers from '@/utils/filters';
import * as ELIcons from '@element-plus/icons-vue';
import '@/router/permission';
import '@/styles/index.scss';
import 'element-plus/dist/index.css';

const app = createApp(App);
for(const iconName in ELIcons) {
    app.component(iconName, ELIcons[iconName]);
}
SvgIcon(app);
fitlers(app);
app.use(store).use(router).use(i18n).mount('#app');
