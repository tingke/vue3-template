const dayjs = require('dayjs');

const filterTimes = (date, format = 'YYYY-MM-DD') => {
    if(!isNull(date)) {
        date = parseInt(date) * 1000;
        return dayjs(date).format(format);
    }
};

const isNull = date => {
    if(!date) return true;
    if(JSON.stringify(date) === '{}') return true;
    if(JSON.stringify(date) === '[]') return true;
    return false;
};

export default app => {
    app.config.globalProperties.$filters = {
        filterTimes
    };
};
