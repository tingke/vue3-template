/*
 * @Author: tingke
 * @Date: 2022-02-13 17:01:23
 * @LastEditors: tingke
 * @LastEditTime: 2022-02-13 17:58:36
 * @FilePath: /vue-template/src/views/users/options.js
 */
export const options = [
    {
        label: 'username',
        prop: 'username'
    },
    {
        label: 'email',
        prop: 'email',
        width: 200
    },
    {
        label: 'mobile',
        prop: 'mobile'
    },
    {
        label: 'role_name',
        prop: 'role_name'
    },
    {
        label: 'mg_state',
        prop: 'mg_state'
    },
    {
        label: 'create_time',
        prop: 'create_time'
    },
    {
        label: 'action',
        prop: 'action',
        width: 200
    }
];
